Rails.configuration.action_mailer.smtp_settings = {
  address: SMTP_ADDRESS,
  port: SMTP_PORT,
  user_name: SMTP_USER_NAME,
  password: SMTP_PASSWORD,
  authentication: SMTP_AUTHENTICATION
}
