# The Mail Service

This is the Mail Service for Ocean. It sends mail using AsyncJobs.

## Installation

First start the common infrastructure, if it isn't already running:
```
  docker-compose -f ../ocean/common.yml up --build
```
Then, to run only the Mail service:
```
  docker-compose -f ../ocean/ocean.yml up --build mail_service
```


## Running the RSpecs

If you intend to run RSpec tests, start the common infrastructure, if it isn't
already running:
```
  docker-compose -f ../ocean/common.yml up --build
```
Then, to run the actual specs:
```
  docker-compose -f ../ocean/ocean.yml run --rm -e LOG_LEVEL=fatal mail_service rspec
```
